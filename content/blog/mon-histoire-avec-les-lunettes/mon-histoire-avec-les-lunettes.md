---
title: 'Mon histoire avec les lunettes. :eyesglasses:'
date: '2021-01-09'
---

En Première, en 2017, j'ai eu ma première paire de lunettes. Je suis myope avec -4,5 de correction. Le style de la monture était très classique, carré de couleur noire :
![black rectangle frame](black-rectangle-frame.jpg)
Photo by [🇨🇭 Claudio Schwarz | @purzlbaum](https://unsplash.com/@purzlbaum?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/s/photos/glasses?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)

Le problème est que ce genre de lunettes sans plaquettes va tomber sur mon nez d'asiatique. L'arête (ou dorsum) de mon nez est trop plat pour que le pont (ce qu'il y a entre les 2 verres) des lunettes s'assoit dessus.

L'année suivante, en 2018, j'ai changé la monture de mes lunettes. La mutuelle me permet de changer de monture et de verres tous les ans. J'ai pris ce modèle-là :
![black oval frame](black-oval-frame.jpg)Photo by [Dayne Topkin](https://unsplash.com/@dtopkin1?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/s/photos/black-glasses?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)

J'ai toujours le même problème, ces lunettes veulent jouer au toboggan sur mon nez T_T

1 an après, j'ai pris une monture qui convient parfaitement à mon visage. Merci à mes amis de m'avoir aidé pour le choix !
C'est la monture dorée et noire avec les plaquettes :
![golden oval frame](golden-oval-frame.jpg)Source : [Betty Rotaru](https://unsplash.com/photos/9O6-vndp1dg)

Mais, la monture glisse toujours sur mon nez... La plaquette est glissante car il y a du sébum qui se produit sur mon nez.
Mon ancien opticien m'a serré les branches des lunettes, en vain. En plus, j'ai la douleur qui apparaît aux oreilles. Je suis allé régler mainte fois, à tel point que je voyais sur le visage de mon opticien qu'il en a marre.

En janvier 2021, un magasin [Optic 2000](https://www.optic2000.com) a repéré que c'est la plaquette qui posait problème. Une opticienne a changé mes plaquettes en silicone gratuitement, comme par magie, ma monture dorée s'assoit confortablement sur mon nez... Depuis tout ce temps, mon ancien opticien n'arrivait pas à repéré ça...

J'ai décidé donc de changer d'opticien. Je deviens officiellement un client de Optic 2000.

## Conclusion

-   Je dois porter les lunettes avec des plaquettes en silicone.

-   Les lunettes dorées conviennent parfaitement à mon visage.

-   Appeler un ami pour choisir les lunettes.

## Références

[Le vocabulaire des lunettes - Pinterest](https://www.pinterest.fr/pin/582582901776459607/)

[Anatomie du nez en vue de trois quarts - Le monde en images](https://monde.ccdmd.qc.ca/ressource/?id=122119)

[Nez : Les différences ethniques - EVAZION ESTHETIQUE](https://www.chirurgienplastique.org/nez-les-differences-ethniques/)
