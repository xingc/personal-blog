---
title: Première neige de 2021
date: 2021-01-16
---

Incroyable, il a neigé à Paris. La neige est sur le sol, sur les voitures, même sur mes chaussures. Les mioches dans la rue jouaient à la bataille de boules de neige ou faisaient un bonhomme de neige. En voyant tout ça, je vois les images de mon adolescence défilées devant moi.
A l'époque de l'école primaire, la neige durait plus d'une semaine. A chaque fois que il y a la sonnerie, mes camarades et moi courions à la cour de récréation pour caresser la neige :).
Maintenant il neige que pendant une seule journée, et jamais le 25 décembre :ç Je ne retrouve plus la magie de Noël.
![snow at the bus staion](snow-at-bus-station.jpg)

Ce jour-là, je marchais dans la rue, il y avait de la buée sur mes lunettes. Je marchais à l'aveugle. C'est la vie d'un myope quand la météo est mauvaise.
![snow](snow.jpg)

Pour certaine fille, c'est la première fois qu'elle caresse la neige :)
