---
title: Mon bilan 2020
date: "2021-01-04"
description: "Mon bilan 2020"
---

L'année 2020 est passée comme un battement de cils. Nous sommes confiné pendant 4 mois en France..

L'économie de l'humanité a été arrêté, le chômage augmente..

2020 s'est passé beaucoup de choses : Covid-19, confinement, l'incendie en Australie... Je vous présente tout ça ! :blush:  

## Janvier

- [#WWIII](https://twitter.com/search?q=%23WWIII&src=typeahead_click) sur Twitter. La 3ème Guerre Mondiale..
- L'incendie en Australie

## Février

Rien à dire.

## Mars

- L'émergence de coronavirus

- La mise en place du confinement dans le monde entier

## Avril

- J'ai commencé à jouer le piano car je m'ennuyais durant le confinement

## Mai

- :notebook: J'ai commencé à écrire un journal intime

- J'ai commencé mon stage informatique de 6 semaines

## Juin

- Le déconfinement en France

## Juillet

- Fin de mon stage informatique et début des vacances d'été

## Août

Rien à dire

## Septembre

- La Rentrée de ma classe BTS 2ème année

## Octobre

- :hospital: J'ai eu l'abcès dentaire :sob:

## Novembre

- Une jolie dentiste m'a mis une couronne qui a coûté 1000€ :sob:

- L'achat d'une [gourde isotherme minimaliste](https://www.chillysbottles.com/fr/products/bottle-monochrome-all-black?sku=B500MOABL), 30€.

## Décembre

- J'ai préparé une petite surprise à mes camarades de classe :blush: pour le nouvel an

## Conclusion

2020 était une année compliqué pour certains,  mais loin de la pire année si nous regardons à l'échelle de l'humanité. Il y a des années où y a les guerres, la famine.

Étant un casanier, le confinement ne me dérange pas tant que ça. :blush:
