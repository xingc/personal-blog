---

title:  J'ai regardé l'Attaque des Titans (進撃の巨人)
date: 2021-01-21

---

Le 31 décembre 2020, j'ai fini l'Attaque des titans. C'est une perle que j'ai trouvé en cette fin d'année. Je suis subitement séduit par l'univers des titans dès l'épisode 1. C'est fou cet anime.

L’épisode 1 est génial. L'introduction de l'univers dure environ 10 minutes, puis l'anime entre directement dans l'action. On voit rapidement à quoi ressemble un titan. Contrairement à certains animes où l'introduction des personnages durent 2 voire 3 épisodes. 
![Colossal titan](colossal_titan.png)

Dès l'épisode 1 on voit des scènes terribles, des gens bouffés par les titans et surtout la mère du protagoniste Eren arracher la tête par un titan.
![mother](mother.png)
C'est une scène traumatisant pour Eren mais aussi pour moi. J'avais la bouche ouverte devant mon écran ! 
![Eren shocked](eren_shocked.png)Je suis entrer dans le corps des personnes, j'avais très peur pour eux, parfois je me dit "ah fait gaffe, y a un titan derrière toi !!"
Je ressens la mélancolique de voir les soldats mourir sous mes yeux.. 
![Titans](titan.jpeg)

Je suis vraiment attiré par l'intrigue. Il me fait poser beaucoup de questions car il y a beaucoup de suspens. A chaque fin d'épisode, j'ai envie de voir l'épisode suivant. Je découvre la vérité en même temps que les personnages.

Je vous recommande vivement de regarder l'Attaque des Titans. À mes yeux, 進撃の巨人 n'est pas un anime, mais un **chef-d'œuvre**. 
