---
title: 'Le sourire de Levi Ackerman'

date: '2021-01-16'
---

Dans l'épisode 10 de la saison 3 de l'Attaque des titans, il s'est passé un truc qui est mémorable.
Pour la première fois, j'ai vu le sourire de [Levi Ackerman](https://attaque-des-titans.fandom.com/fr/wiki/Liva%C3%AF_Ackerman).

Pour rappel, Levi Ackerman est le caporal-chef d'une armée dans l'animé [Attaque des titans](https://fr.wikipedia.org/wiki/L%27Attaque_des_Titans). Il est considéré comme un homme puissant, il s'en sort toujours dans les pires situations. C'est pourquoi il n'est toujours pas mort malgré les morts consécutives de ses camarades soldats devant les titans. En résumé, il est très fort.

Ce mec-là a constamment un regard intimidant et un visage froid et neutre.
Il est comme ça depuis 2 saisons..

![levi-ackerman-look](levi-ackerman-look.jpeg)

![levi-ackerman-look](levi-ackerman-look2.jpeg)
[Source](https://www.thpanorama.com/blog/entretenimiento/levi-ackerman-historia-personalidad-y-frases.html)

Pour la première fois, j'ai vu le sourire de Levi Ackerman dans l'épisode 10 de la saison 3. Honnêtement, son sourire sucré me fait fondre.. Je suis tellement attiré par ce personnage..

![levi smiles](levi-smile.png)

J'avais la même expression que ces gens-là devant le sourire de Levi.

![people shocking](people-shocking.png)
