import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import SEO from '../components/seo'
import Image from 'gatsby-image'
import styles from './about.module.css'
import { navigate } from 'gatsby'

export default function About({ data, location }) {
    const siteTitle = data.site.siteMetadata?.title || `Title`
    const author = data.site.siteMetadata?.author
    const avatar = data?.avatar?.childImageSharp?.fixed

    /* see: https://github.com/reach/router/issues/44 */
    const goBack = function () {
        navigate(-1)
    }

    return (
        <Layout location={location} title={siteTitle} isFullscreen={true}>
            <SEO title="Propos" />

            <section className={styles.wrapper}>
                <span className={styles.backLink}>
                    <a onClick={goBack}>Retour</a>
                </span>
                <section className={styles.avatar}>
                    {avatar && (
                        <Image
                            fixed={avatar}
                            alt={author?.name || ``}
                            imgStyle={{
                                borderRadius: `50%`,
                            }}
                        />
                    )}
                    <p
                        style={{
                            fontSize: 'var(--fontSize-0)',
                            marginBottom: '1px',
                        }}
                    >
                        Merci à Anami d'avoir pris cette magnifique photo !
                    </p>
                    <p>Date de création : 2 janvier, 2021</p>
                </section>
                <section className={styles.infos}>
                    <p>
                        Jeune chinois vivant en France, je parle de tout ce qui
                        me passe par la tête. Mon blog un endroit intime où je
                        peux m'exprimer librement, délibérer le regard des
                        autres.
                    </p>
                    <p>
                        Passion : utiliser le pouvoir de l'informatique pour
                        répondre à un besoin
                    </p>
                    <p>
                        星 est mon prénom en caractère chinois. Ça veux
                        littéralement dire l'étoile. J'ai ajouté un point 。à la
                        fin du caractère parce que c'est stylé :).
                    </p>
                </section>
            </section>
        </Layout>
    )
}

export const pageQuery = graphql`
    query {
        avatar: file(absolutePath: { regex: "/profile-pic.jpg/" }) {
            childImageSharp {
                fixed(width: 250, height: 250, quality: 95) {
                    ...GatsbyImageSharpFixed
                }
            }
        }
        site {
            siteMetadata {
                title
                author {
                    name
                    summary
                }
            }
        }
    }
`
